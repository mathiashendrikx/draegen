FROM python:3.7-alpine
COPY hello.py /srv/hello.py
CMD python3 /srv/hello.py
