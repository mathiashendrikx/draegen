#!/usr/bin/python
from http.server import BaseHTTPRequestHandler, HTTPServer

my_string: bytes = b"Hello World! ;]"


class myHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(my_string)
        return


try:
    server = HTTPServer(('', 8080), myHandler)
    print('Started httpserver on port 8080')

    server.serve_forever()

except KeyboardInterrupt:
    print('Key received, shutting down!')
    server.socket.close()
